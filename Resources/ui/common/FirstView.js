//FirstView Component Constructor
function FirstView() {
	// create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView();

	// Create TableView ( http://developer.appcelerator.com/apidoc/mobile/1.8.2/Titanium.UI.TableView-object )
	var tableView = Titanium.UI.createTableView();
	
	// Activity Indicator ( http://docs.appcelerator.com/titanium/latest/#!/api/Titanium.UI.ActivityIndicator )
	var indicatirStyle = '';
    if( Titanium.Platform.osname == 'iphone' ){
        indicatirStyle = Titanium.UI.iPhone.ActivityIndicatorStyle.DARK;
    } else {
        indicatirStyle = Titanium.UI.ActivityIndicatorStyle.DARK;
    }
	var indicator = Titanium.UI.createActivityIndicator({
	    'message' : L('loading'),
	    'color' : 'black',
	    'style' : indicatirStyle,
	    'width' : Titanium.UI.SIZE,
	    'height' : Titanium.UI.SIZE,
	    'font' : {
    	    	fontFamily : 'Helvetica Neue',
    	    	fontSize : '20sp',
	    }
	});
	self.add(indicator);
	indicator.show();

	// Create HTTPClient ( http://developer.appcelerator.com/apidoc/mobile/1.8.2/Titanium.Network.createHTTPClient-method.html )
	var xhr = Titanium.Network.createHTTPClient({
		// HTTPClient onload
		'onload' : function() {
			// Get Rss element
			var doc = this.responseXML.documentElement

			// Get element item tag data
			var element = doc.getElementsByTagName('item');

			// Generate data array
			var data = [];

			// Get item data content
			for (var i = 0; i < element.length; i++) {
			    // tableView add row example 1 ( http://docs.appcelerator.com/titanium/latest/#!/api/Titanium.UI.TableView-method-appendRow )
			    // tableView.appendRow({
				// 'title' : element.item(i).getElementsByTagName('title').item(0).text,
				// 'content' : element.item(i).getElementsByTagName('content:encoded').item(0).text,
				// 'color' : 'black',
				// 'font' : {
				    // 'fontSize' : '20sp'
				// }
			    // });
			    
			    // tableView add row example 2 ( http://docs.appcelerator.com/titanium/latest/#!/api/Titanium.UI.TableView-property-data )
			    
			    data.push({
				// TableRow title color
				'color' : '#000000',
				// TableRow title
				'title' : element.item(i).getElementsByTagName('title').item(0).text,
				// Row data content
				'content' : element.item(i).getElementsByTagName('content:encoded').item(0).text,
				'font' : {
				    'fontSize' : '20sp'
					}
			    })
			    //*/
			}
			// Set TableView row data
			tableView.data = data;
			indicator.hide();

			// TableView click event
			tableView.addEventListener('click', function(event) {
				// Create new Window
				var win = Titanium.UI.createWindow();
				// Create WebView
				var web = Titanium.UI.createWebView({
					// WebView loading show indicator
					'loading' : true,
					// WebView load html data to content
					'html' : event.rowData.content,
					'top': 0,
					'left': 0,
					'bottom': 0,
					'right': 0,
					'width': '100%',
					'height':'auto',
					'scalesPageToFit' : false,
				});

				// WebView add to Window
				win.add(web);

				if( Titanium.Platform.osname == 'iphone' ){
        				// Create back button
        				var back = Ti.UI.createButton({
        					// Button title
        					'title' : 'Back',
        					// Button style
        					'style' : Titanium.UI.iPhone.SystemButtonStyle.PLAIN,
        				});
        				// Back button click event
        				back.addEventListener('click', function(event) {
        					// Close new Window
        					win.close();
        				});
        				// Set back button to Nav left
        				win.setLeftNavButton(back);
				}

				// Window open
				win.open({
					// This view in parent window top
					'modal' : true,
				});
			});
			
			// Window add TableView
			self.add(tableView);
		},
		'onerror' : function(){
		    indicator.hide();
		},
		// HTTPClient timeout
		'timeout' : 5000,
	});

	// HTTPClient open
	xhr.open('GET', 'http://www.appsgoo.com/feed');
	xhr.send();

	return self;
}

module.exports = FirstView;
